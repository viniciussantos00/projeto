	</body>
		<footer class="page-footer text-center font-small mt-4 wow fadeIn">
			<div class="footer-copyright py-3"> Todos os direitos reservados © 2019 Dois Irmãos</div>
		</footer>

		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.4.1.min.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js')?>"></script>
		<script type="text/javascript"> new WOW().init(); </script>
</html>