CREATE DATABASE lp2_vinicius CHARSET utf8;

CREATE TABLE options (
  id 	          int(11) 	   NOT NULL AUTO_INCREMENT,
  option_name 	varchar(255) NOT NULL,
  option_value 	text         NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO options (option_name, option_value) values ('user_login','administrador'), ('user_email','administrador@ifspgru.com.br'), ('user_senha', '$2y$10$tBRVp8UTCk1jDHPFe9TF8e6Q9kkUXPIqRSVAJPlBXfi01mkN/C0sK'), ('setup_executado', '1');

CREATE TABLE remedio (
	id 	        int(11) 		NOT NULL AUTO_INCREMENT,
	nome 		varchar(100) 	NOT NULL,
	descricao	varchar(100) 	NOT NULL,
	preco		text        	NOT NULL,
	imagem 		varchar(50) 	NOT NULL,
	PRIMARY KEY (id)
) ;

INSERT INTO remedio (nome, descricao, preco, imagem) 
values ('Ibuprofeno', 'Alívio temporário da dor leve a moderada associada a resfriado comum, dor de cabeça', '8', 'ibuprofeno.jpg'),
       ('Dipirona Sódica', 'Alívio temporário da dor de cabeça leve a moderada', '5', 'dipirona.jpg'),
       ('Diclofenaco Dietilamônio', 'Age sobre Contusões e dores musculares.', '15', 'diclofenaco_dietilamonio.jpg'),
       ('Dimetiliv', 'Indicado como antigases, também para os sintomas de estômago pesado, inchaço, estufamento e desconforto causado     pelos gases.', '4', 'dimetiliv.jpg'),
       ('Glicerin', 'Possui ação laxativa, emoliente e trata de forma natural a prisão de ventre e a constipação intestinal.', '10',      'glicerin.jpg'),
       ('Acetilcisteina', 'Contra Tosse e expectoração.', '9', 'acetilcisteina.jpg');