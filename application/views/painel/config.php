<br/><br/><br/>
<body>
    <div class="container"><br/>
    <?php 
        if($msg = get_msg()){
            echo '<div class="alert alert-danger">'.$msg.'</div>';   
        } 
    ?>
        <div class="row">
            <div class="col-md-6 mx-auto border">
                <form method="POST" class="text-center border-light p-5">
                    <p class="h4 mb-4">Alterar cadastro</p>
                    <div class="form-row mb-4">
                    <input type="text" value="<?= set_value('login'); ?>" id="login" class="form-control mb-4" name="login" placeholder="Login">
                    <input type="text" value="<?= set_value('email'); ?>" id="email" class="form-control mb-4" name="email" placeholder="E-mail">
                    <input type="password" id="senha" class="form-control mb-4" name="senha" placeholder="Senha (Deixar em branco para não alterar)">
                    <input type="password" id="senha2" class="form-control mb-4" name="senha2" placeholder="Confirme a sua senha">
                    <button class="btn btn-danger btn-block" type="submit">Alterar</button>
                </form>
            </div>
        </div>
    </div>
</div>
