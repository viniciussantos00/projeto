<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    if(!function_exists('set_msg')){
        //setar uma msg via session para ser lida futuramente
        function set_msg($msg=NULL){
            $ci = & get_instance();
            $ci->session->set_userdata('aviso', $msg);
        }
    }

    if(!function_exists('get_msg')){
        //retorna a msg definida em set_msg
        function get_msg($destroy=TRUE){
            $ci = & get_instance();
            $retorno = $ci->session->userdata('aviso');
            if($destroy) $ci->session->unset_userdata('aviso');
            return $retorno;
        }

    }

    if(!function_exists('verifica_login')){
        function verifica_login(){
            $ci = & get_instance();
            if($ci->session->userdata('logged') != TRUE){
                return FALSE;
            }else{
                return TRUE;
            }
        }
    }

    if(!function_exists('config_upload')){
        function config_upload($path = './upload/', $types='jpg|png', $size="512"){
            $config['upload_path'] = $path;
            $config['allowed_types'] = $types;
            $config['max_size'] = $size;

            return $config;
        }
    }

    if(!function_exists('to_bd')){
        function to_bd($string = NULL){
            return htmlentities($string);
        }
    }
?>