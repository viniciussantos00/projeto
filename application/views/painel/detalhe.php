<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <br/><br/><br/>
  <body>    
    <div class="container">
      <div class="row">
        <h1><?php echo $produto->nome?></h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Imagem</th>
                    <th class="text-center">Descrição</th>
                    <th class="text-right">Preço</th>
                </tr>
            </thead>
            <?php    
                    echo '<tr>';
                        echo '<td> <img src="'.base_url('upload/'.$produto->imagem).'" class="thumb-edicao" /> </td>';
                        echo '<td>'.$produto->descricao.'</td>';
                        echo '<td class="text-right">'.$produto->preco.'</td>'; 
                    echo '</tr>';
            ?>
        </table>

        <a href="<?= base_url('produto')?>" role="button" class="btn btn-danger">Voltar</a>
      </div>
    </div>
