<body>
  <br/><br/><br/><br/>
  <div class="container">
    <div class="row">
      <h1>Lista de produtos: </h1>
      <div class="card-deck">
      <?php
        $contador = 0;
        foreach ($produtos as $produto){
          echo '<div class="card">';
            echo '<img class="card-img-top" src="'.base_url('upload/'.$produto->imagem).'" width="200" height="350">';
            echo '<div class="card-body">';
              echo '<h5 class="card-title">'.$produto->nome.'</h5>';
              echo '<p class="card-text">'.$produto->descricao.'</p>';
              echo '<p class="card-text"><small class="text-muted">R$ '.$produto->preco.'</small></p>';
              echo '<a href="setup/detail_produto/'.$produto->id.'" title="Detalhes" class="btn btn-primary"><span class="far fa-eye" aria-hidden="true"></span></a>';
            echo '</div>';
          echo '</div>';
          $contador++;
          if($contador == 3 ){
            echo '</div></div><br/><div class="row"><div class="card-deck">';
            $contador = 0;
          }
        }
      ?> 
      </div>
    </div>
  </div>