<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->helper('form');    
        $this->load->model('Option_Model', 'option');
        
    }

    public function index(){
            //rodar tela de login
            redirect('setup/login');
    }

    public function alterar(){

        verifica_login();
               
        $this->option->alterar();    

        $_POST['login'] = $this->option->get_option('user_login');
        $_POST['email'] = $this->option->get_option('user_email');
        $dados['titulo'] = 'Painel - Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('painel/navbar_painel');
        $this->load->view('painel/config');
        $this->load->view('painel/footer_painel');
        
    }

    /*public function cadastro(){  
        
        $this->option->registrar();

        $dados['titulo'] = 'Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('painel/setup');
        $this->load->view('common/footer');

    }*/

    public function login(){

        if(verifica_login() == TRUE){
            set_msg('<p>Usuário não logado</p>');
            redirect('setup/bemvindo');
        }else{

            $this->option->logar();
            
            $dados['titulo'] = 'Dois Irmãos';
            $this->load->view('painel/topo_painel', $dados);
            $this->load->view('exception/navbar_login');
            $this->load->view('painel/login');
            $this->load->view('common/footer');
        }
    }

    public function logout(){
        
        $this->session->unset_userdata('logged');    
        $this->session->unset_userdata('user_login');    
        $this->session->unset_userdata('user_email');
        set_msg('</p>Deslogado com sucesso</p>');
        redirect('setup/login');  

    }

    public function produto(){

        $this->load->model('listar_model', 'listar');
        $dados['produtos'] = $this->listar->getProdutos();    

        $dados['titulo'] = 'Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('painel/navbar_painel');
        $this->load->view('painel/listar', $dados);
        $this->load->view('common/footer');
    }

    public function add(){
        $msg = $this->option->produto_validate();

        $dados['titulo'] = 'Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('painel/navbar_painel');
        $this->load->view('painel/addproduto');
        $this->load->view('common/footer');
        
    }

    public function edit($id=NULL){
        $msg = $this->option->produto_edit($id);

        $query = $this->option->getProdutoId($id);
        $dados['produto'] = $query;

        $dados['titulo'] = 'Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('painel/navbar_painel');
        $this->load->view('painel/editproduto', $dados);
        $this->load->view('common/footer');
    }

    public function delete($id){

        $this->load->model('listar_model', 'listar');

        $query = $this->option->getProdutoId($id);

        if($query != NULL){
            $this->listar->deleteProdutos($query->id);
            redirect('setup/produto');
        }
    }

    public function detail_produto($id){

        $this->option->produto_edit($id);

        $query = $this->option->getProdutoId($id);
        $dados['produto'] = $query;

        $dados['titulo'] = 'Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('common/navbar');
        $this->load->view('painel/detalhe', $dados);
        $this->load->view('common/footer');
    }

    public function bemvindo(){
        
        verifica_login();
           
        $dados['titulo'] = 'Painel - Dois Irmãos';
        $this->load->view('painel/topo_painel', $dados);
        $this->load->view('painel/navbar_painel');
        $this->load->view('painel/bemvindo');
        $this->load->view('painel/footer_painel');
    }

}
?>