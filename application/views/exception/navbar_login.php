<body>

  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

      <a class="navbar-brand" href="<?php echo base_url();?>">
        <i class="fas fa-male"></i>
        <i class="fas fa-male mr-2"></i>
        <strong> Dois Irmãos </strong>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>"> Página Inicial </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('sobre');?>"> Sobre Nós </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('produto');?>"> Produtos </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('contato');?>"> Contato </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('trabalhe');?>"> Trabalhe Conosco </a>
        </ul>

        <ul class="navbar-nav nav-flex-icons">  
          <li class="nav-item">
            <a href="https://www.facebook.com/" class="nav-link" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com/" class="nav-link" target="_blank">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>