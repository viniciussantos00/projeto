<main>
<br/><br/>
  <div class="container">
    <section class="mt-5 wow fadeIn">
      <div class="row">
        <div class="col-md-6 mb-4">
          <img src="<?= base_url('assets/img/prateleira.jpg')?>" class="img-fluid z-depth-1-half" alt="">
        </div>

        <div class="col-md-6 mb-4">
          <h3 class="h3 mb-3">Desconto em diversos produtos da loja</h3>
          <p>Acumule pontos e troque por descontos.</p>
          <hr>
          <p> Garanta descontos especiais em seus medicamentos, cosméticos ou produtos de higiene pessoal, através do nosso sistema de benefícios para participantes do nosso clube do consumidor.</p>
        </div>
      </div>
    </section>

    <hr>

    <section class="mt-5 wow fadeIn">
      <div class="row">
        <div class="col-md-6 mb-4">
          <h3 class="h3 mb-3">Frete grátis em todo o site</h3>
          <p> Com o clube de vantagens você recebe o benefício de obter frete grátis em toda a loja para o Brasil inteiro.</p> 
        </div>

        <div class="col-md-6 mb-4">
          <img src="<?= base_url('assets/img/entrega.jpg')?>" class="img-fluid z-depth-1-half" alt="">
        </div>
      </div>
    </section>
    
    <hr class="my-5">
    <section>
      <h2 class="my-5 h3 text-center">Escolha Dois Irmãos</h2>
      <div class="row features-small mb-5 mt-3 wow fadeIn">
        <div class="col-md-4">
          <div class="row">
            <div class="col-2">
              <i class="fas fa-check-circle fa-2x indigo-text"></i>
            </div>
            <div class="col-10">
              <h6 class="feature-title"> Medicamentos de qualidade </h6>
              <p class="grey-text"> Garantimos a qualidade dos nossos medicamentos através do nosso sistema de parceria com os melhores laboratórios do país.</p>
              <div style="height:15px"></div>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-2">
              <i class="fas fa-check-circle fa-2x indigo-text"></i>
            </div>
            <div class="col-10">
              <h6 class="feature-title">O melhor preço</h6>
              <p class="grey-text"> Fazemos o melhor preço dentre as farmácias online</p>
              <div style="height:15px"></div>
            </div>
          </div>
        </div>

        <div class="col-md-4 flex-center">
          <img src="<?= base_url('assets/img/dois_irmaos.jpg')?>" alt="MDB Magazine Template displayed on iPhone" class="z-depth-0 img-fluid">
        </div>

        <div class="col-md-4 mt-2">
          <div class="row">
            <div class="col-2">
              <i class="fas fa-check-circle fa-2x indigo-text"></i>
            </div>
            <div class="col-10">
              <h6 class="feature-title">A melhor entrega</h6>
              <p class="grey-text"> Nos comprometemos a entregar em todo o Brasil e com frete grátis. </p>
              <div style="height:15px"></div>
            </div>
          </div>
          
          <br/>

          <div class="row">
            <div class="col-2">
              <i class="fas fa-check-circle fa-2x indigo-text"></i>
            </div>
            <div class="col-10">
              <h6 class="feature-title"> Atendimento ao cliente personalizado </h6>
              <p class="grey-text"> Aqui cada cliente é especial para nós, pensando nisso, nós possuímos um time de especialistas disponíveis 24h para sanar qualquer dúvida que possa surgir. </p>
              <div style="height:15px"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</main>