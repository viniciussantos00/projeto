<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <br/><br/><br/><br/>
  <body>    
    <div class="container">
      <div class="row">
        <h1>Lista de produtos</h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                  <th class="text-center">Produto</th>
                  <th class="text-center">Descrição</th>
                  <th class="text-right">Preço</th>
                  <th class="text-center">Açoes</th>
                </tr>
            </thead>

            <?php
                $contador = 0;
                foreach ($produtos as $produto)
                {        
                    echo '<tr>';
                      echo '<td>'.$produto->nome.'</td>'; 
                      echo '<td>'.$produto->descricao.'</td>';
                      echo '<td class="text-right">R$ '.$produto->preco.',00</td>'; 
                      echo '<td class="text-center">';
                        echo '<a href="edit/'.$produto->id.'" title="Editar cadastro" class="btn btn-primary"><span class="fas fa-pencil-alt" aria-hidden="true"></span></a>';
                        echo ' <a href="delete/'.$produto->id.'" title="Apagar cadastro" class="btn btn-danger"><span class="fas fa-eraser" aria-hidden="true"></span></a>';
                      echo '</td>'; 
                    echo '</tr>';
                $contador++;
                }
            ?>

        </table>

        <div class="row">
          <div class="col-md-12">
            Todal de Registro: <?php echo $contador ?>
          </div>
          <div class="col-md-12">
          <a href="<?= base_url('setup/add');?>" title="Apagar cadastro" class="btn btn-danger">Novo Produto</a>  
          </div>
        </div>
      </div><br/><br/>
    </div>
