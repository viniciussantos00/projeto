<body>
    <div class="view full-page-intro" style="background-image: url('<?=base_url('assets/img/fundo.jpg')?>'); background-repeat: no-repeat; background-size: cover;">

    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container">
            <div class="row wow fadeIn">
                <div class="col-md-6 mb-4 white-text text-center text-md-left">
                    <h1 class="display-4 font-weight-bold">Venha fazer parte do nosso clube de consumidores</h1>
                    <hr class="hr-light">
                    <p class="mb-4 d-none d-md-block">
                        <strong>Toda a comodidade e praticidade de uma farmácia ao alcance das suas mãos. Fazendo parte do nosso clube você acumula pontos que podem ser trocados por descontos em compras futuras, além de obter frete grátis para todo o país.</strong>
                    </p>

                    <a target="_blank" href="<?php echo base_url('produto');?>" class="btn btn-danger btn-lg"> Ir para a loja
                        <i class="fas fa-shopping-cart ml-2"></i>
                    </a>

                </div>

                <div class="col-md-6 col-xl-5 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <form name="" method="POST">
                                <h3 class="dark-grey-text text-center"> <strong>Cadastre-se</strong></h3>
                                <hr>

                                <div class="md-form">
                                    <i class="fas fa-user-circle prefix grey-text"></i>
                                    <input type="text" id="login" name="login" class="form-control" placeholder="Nome">
                                </div>

                                <div class="md-form">
                                    <i class="far fa-envelope prefix grey-text"></i>
                                    <input type="text" id="email" name="email" class="form-control" placeholder="E-mail">
                                </div>

                                <div class="md-form">
                                    <i class="fas fa-key prefix grey-text"></i>
                                    <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha">
                                </div>

                                <div class="md-form">
                                    <i class="fas fa-key prefix grey-text"></i>
                                    <input type="password" id="senha2" name="senha2" class="form-control" placeholder="Repita a senha">
                                </div>

                                <div class="text-center">
                                    <button class="btn btn-danger btn-block">Cadastrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>