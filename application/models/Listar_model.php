<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar_model extends CI_Model {
    
    public function getProdutos(){

        $query = $this->db->get('remedio');
        return $query->result();
    }

    public function addProdutos($dados){
        if($dados != NULL){
            $this->db->insert('remedio', $dados);
            return true;
        }
    }

    public function editProdutos($dados, $id){
        if($dados != NULL && $id != NULL){
            $this->db->update('remedio', $dados, array('id' => $id));
            return true;
        }
    }

    public function deleteProdutos($id){
        if($id != NULL){
            $this->db->delete('remedio', array('id' => $id));
        }
    }

}