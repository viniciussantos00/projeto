<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'session', 'form_validation', 'util/CI_Object');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'funcoes');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();
