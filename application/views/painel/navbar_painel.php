<body>

  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

      <a class="navbar-brand" href="<?php echo base_url();?>" target="_blank">
        <i class="fas fa-male"></i>
        <i class="fas fa-male mr-2"></i>
        <strong> Painel - Dois Irmãos </strong>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('');?>" target="_blank"> Visualizar Site </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('setup/produto');?>"> Produtos </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('setup/alterar');?>"> Configurações </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('setup/logout');?>"> Sair </a>
        </ul>

        <ul class="navbar-nav nav-flex-icons">  
          <li class="nav-item">
            <a href="https://www.facebook.com/" class="nav-link" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com/" class="nav-link" target="_blank">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>