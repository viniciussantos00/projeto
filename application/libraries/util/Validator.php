<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Validator extends CI_Object{

    public function valida_alt_adm(){

        $this->form_validation->set_rules('login', 'nome',  'trim|required|min_length[6]');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'senha', 'trim|min_length[6]');
        if(isset($_POST['senha']) && $_POST['senha'] != ''){
            $this->form_validation->set_rules('senha2', 'repita a senha', 'trim|required|min_length[6]|matches[senha]');
        }

        return $this->form_validation->run();

    }

    public function valida_cadastro_adm(){

        $this->form_validation->set_rules('login',  'nome',           'trim|required|min_length[6]');
        $this->form_validation->set_rules('email',  'email',          'trim|required|valid_email');
        $this->form_validation->set_rules('senha',  'senha',          'trim|required|min_length[6]');
        $this->form_validation->set_rules('senha2', 'repita a senha', 'trim|required|min_length[6]|matches[senha]');

        return $this->form_validation->run();
        
    }

    public function valida_login(){
        
        $this->form_validation->set_rules('login', 'nome',  'trim|required|min_length[6]');
        $this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[6]');

        return $this->form_validation->run();

    }

    public function valida_produto(){

        $this->form_validation->set_rules('nome',      'nome',      'trim|required|min_length[6]');
        $this->form_validation->set_rules('descricao', 'descricao', 'trim|required|min_length[10]');
        $this->form_validation->set_rules('preco',     'preço',     'trim|required|numeric');

        return $this->form_validation->run();
        
    }

    public function valida_contato(){

        $this->form_validation->set_rules('nome',     'Nome',     'trim|required');
        $this->form_validation->set_rules('email',    'Email',    'trim|required|valid_email');
        $this->form_validation->set_rules('assunto',  'Assunto',  'trim|required');
        $this->form_validation->set_rules('mensagem', 'Mensagem', 'trim|required');

        return $this->form_validation->run();
    }

}