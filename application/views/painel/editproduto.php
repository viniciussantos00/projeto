<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <br/><br/><br/>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                    <h1>Alterar produto</h1>      
                </div>
            </div>
        <?php 
            if($msg = get_msg()){
               echo '<div class="alert alert-danger">'.$msg.'</div>';
            } ?>
        <form name="form_add" method="post" enctype="multipart/form-data">
          
            <div class="row">
                <div class="col-md-8">
                    <label>Nome</label>
                    <input type="text" name="nome" value="<?php echo $produto->nome; ?>" class="form-control">
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <label>Preço</label>
                    <input type="text" name="preco" value="<?php echo $produto->preco;?>" class="form-control">
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <label>Descrição</label>
                    <input type="text" name="descricao" value="<?php echo $produto->descricao;?>" class="form-control">
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <label>Imagem: </label>
                    <input type="file" name="imagem" class="form-control" value="<?php echo $produto->imagem;?>">
                </div>
            </div>

            <br/>
            <div class="row">
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary">Alterar</button>
                    <a href="<?= base_url('setup/produto'); ?>" role="button" class="btn btn-primary">Voltar</a>
                </div>
            </div>
        </form>
      </div>
    </div>