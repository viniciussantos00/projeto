<br/><br/><br/>
<body>
  <div class="container"><br/>
  <?php 
    if($msg = get_msg()){
      echo '<div class="alert alert-danger">'.$msg.'</div>';
  } ?>
  <div class="row">
    <div class="col-md-6 mx-auto border">
      <form method="POST" class="text-center border-light p-5">
        <p class="h4 mb-4">Fale Conosco</p>
        <div class="form-row mb-4">
          <input type="text" value="<?= set_value('nome'); ?>" id="nome" class="form-control mb-4" name="nome" placeholder="Nome">
          <input type="text" value="<?= set_value('email'); ?>" id="email" class="form-control mb-4" name="email" placeholder="E-mail">
          <input type="text" value="<?= set_value('assunto'); ?>" id="assunto" class="form-control mb-4" name="assunto" placeholder="Assunto">
          <input type="text" value="<?= set_value('mensagem'); ?>" id="mensagem" class="form-control mb-4" name="mensagem" placeholder="Mensagem">
          <button class="btn btn-danger btn-block" type="submit">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>
