<body><br/><br/><br/><br/><br/><br/>
    <div class="container"><br/>
    <?php 
    if($msg = get_msg()){
                        echo '<div class="alert alert-danger">'.$msg.'</div>';
                    } ?>
        <div class="row">
            <div class="col-md-6 mx-auto border">
                <form method="POST" class="text-center border-light p-5">
                    <p class="h4 mb-4">Login</p>
                    <div class="form-row mb-4">
                    <input type="text" value="<?= set_value('login'); ?>" id="login" class="form-control mb-4" name="login" placeholder="Usuario">
                    <input type="password" id="senha" class="form-control mb-4" name="senha" placeholder="Senha">
                    <button class="btn btn-danger btn-block" type="submit">LogIn</button>
                </form>
            </div>
        </div>
    </div>
</div><br/><br/><br/>
