<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Cliente extends CI_Controller{

        function __construct(){
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Option_Model', 'option');
        }
        
        public function index(){
            
            $this->option->registrar();
            
            $dados['titulo'] = 'Dois Irmãos';
            $this->load->view('common/topo', $dados);
            $this->load->view('common/navbar');
            $this->load->view('cliente/page_intro.php');
            $this->load->view('common/footer');
        }

        public function produto(){

            $this->load->model('listar_model', 'listar');
            $dados['produtos'] = $this->listar->getProdutos();  

            $dados['titulo'] = 'Dois Irmãos';
            $this->load->view('common/topo', $dados);
            $this->load->view('common/navbar');
            $this->load->view('produto/produto', $dados);
            $this->load->view('produto/footer_produto');
        }

        public function contato(){

            $this->option->contato();

            $dados['titulo'] = 'Dois Irmãos';
            $this->load->view('contato/topo', $dados);
            $this->load->view('common/navbar');
            $this->load->view('contato/contato');
            $this->load->view('common/footer');
        }

        public function sobre(){

            $dados['titulo'] = 'Dois Irmãos';
            $this->load->view('common/topo', $dados);
            $this->load->view('common/navbar');
            $this->load->view('cliente/sobre');
            $this->load->view('common/footer');
        }
        
    }

?>