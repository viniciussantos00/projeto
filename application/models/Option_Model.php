<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_Model extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->load->library('util/Validator');
        $this->load->model('Listar_model', 'listar');
    }

    public function alterar(){

        if($this->validator->valida_alt_adm()){
            $dados_form = $this->input->post();
            $this->update_option('user_login', $dados_form['login']);
            $this->update_option('user_email', $dados_form['email']);
            if(isset($dados_form['senha']) && $dados_form['senha'] != ''){
                $this->update_option('user_senha', password_hash($dados_form['senha'], PASSWORD_DEFAULT));
            }
            return set_msg('<p>Dados alterados com sucesso!</p>');
        }
        else{
            if(validation_errors()){
                return set_msg(validation_errors());
            }
        }
    }

    public function registrar(){

        if($this->validator->valida_cadastro_adm()){
            $dados_form = $this->input->post();
            $inserido = $this->update_option('user_login', $dados_form['login']);
            $this->update_option('user_email', $dados_form['email']);
            $this->update_option('user_senha', password_hash($dados_form['senha'], PASSWORD_DEFAULT));
            $this->update_option('setup_executado', 1);
            
            if($inserido){
                set_msg('<p>Cadastro concluído, faça o login para utilizar o sistema</p>');
                return redirect('setup/login');
            }
        }
        else{
            if(validation_errors()){
                return set_msg(validation_errors());
            }
        }

    }

    public function logar(){

        if($this->validator->valida_login()){
            $dados_form = $this->input->post();
            if($this->get_option('user_login') == $dados_form['login']){ 
                if(password_verify($dados_form['senha'], $this->get_option('user_senha'))){ 
                    $this->session->set_userdata('logged', TRUE);
                    $this->session->set_userdata('user_login', $dados_form['login']);
                    $this->session->set_userdata('user_email', $this->get_option('user_email'));
                    return redirect('setup/bemvindo');   
                }else{
                    return set_msg('<p>Senha Inexistente</p>');
                }
            }else{
                return set_msg('<p>Usuário Inexistente</p>');
            }
        }
        else{
            if(validation_errors()){
                return set_msg(validation_errors());
            }
        }

    }

    public function get_option($option_name){

        $this->db->where('option_name', $option_name);
        $query = $this->db->get('options', 1);
        if($query->num_rows() == 1){
            $row = $query->row();
            return $row->option_value;
        }
        else{
            return NULL;
        }
    }

    private function update_option($option_name, $option_value){
        
        $this->db->where('option_name', $option_name);
        $query = $this->db->get('options', 1);
        if($query->num_rows() == 1){
            //já existe, devo atualizar
            $this->db->set('option_value', $option_value);
            $this->db->where('option_name', $option_name);
            $this->db->update('options');
            return 1;
        }
        else{
            //não existe, devo inserir
            $dados = array(
                'option_name' => $option_name,
                'option_value' => $option_value
            );
            $this->db->insert('options', $dados);
            return $this->db->insert_id();
        }

    }

    public function produto_validate(){

        if($this->validator->valida_produto()){
            $this->load->library('upload', config_upload());
            if($this->upload->do_upload('imagem')){
                $dados_upload = $this->upload->data();
                $dados_form =  $this->input->post();
                $dados_insert['nome'] = to_bd($dados_form['nome']);
                $dados_insert['preco'] = to_bd($dados_form['preco']);
                $dados_insert['descricao'] = to_bd($dados_form['descricao']);
                $dados_insert['imagem'] = $dados_upload['file_name'];

                if($id = $this->listar->addProdutos($dados_insert)){
                    return set_msg('<p>Produto adicionado com sucesso</p>');
                }else{
                    set_msg('<p>Produto não Cadastrado!</p>');
                }
               
            }else{
                $msg = $this->upload->display_errors();
                $msg .= '<p>São Permitidos arquivos JPG e PNG de até 512KB.</p>';
                set_msg($msg);
            }
            
        }else{
            if(validation_errors()){
                return set_msg(validation_errors());
            }
        }
    }

    public function getProdutoId($id){

        if($id != NULL){
            $this->db->where('id', $id);
            $this->db->limit(1);
            $query = $this->db->get('remedio');
            return $query->row();
        }
    }

    public function produto_edit($id){

        if($this->validator->valida_produto()){
            $dados_form =  $this->input->post();
            $dados_insert['nome'] = $dados_form['nome'];
            $dados_insert['preco'] =  $dados_form['preco'];
            $dados_insert['descricao'] = $dados_form['descricao'];

            $this->load->library('upload', config_upload());
            if($this->upload->do_upload('imagem')){
                $dados_upload = $this->upload->data();
                $dados_insert['imagem'] = $dados_upload['file_name'];
            }

            if($id = $this->listar->editProdutos($dados_insert, $id)){
                return set_msg('<p>Produto alterado com sucesso</p>');
            }else{
                set_msg('<p>Produto não alterado!</p>');
            }
        }else{
            if(validation_errors()){
                return set_msg(validation_errors());
            }
        }
    }

    public function contato(){
        if($this->validator->valida_contato()){
            set_msg('<p>Mensagem enviada com sucesso!</p>'); 
        }else{
            if(validation_errors()){
                set_msg(validation_errors());
            }
        }
    }
}
?>