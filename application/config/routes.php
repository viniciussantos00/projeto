<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'cliente';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['produto'] = 'cliente/produto';
$route['contato'] = 'cliente/contato';
$route['sobre'] = 'cliente/sobre';
$route['trabalhe'] = 'cliente/trabalhe';
$route['login'] = 'setup/login';
$route['painel'] = 'setup/login';