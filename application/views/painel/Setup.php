<body>
    <div class="container"><br/>
    <?php 
    if($msg = get_msg()){
                        echo '<div class="alert alert-danger">'.$msg.'</div>';
                    } ?>
        <div class="row">
            <div class="col-md-6 mx-auto border">
                <form method="POST" class="text-center border-light p-5">
                    <p class="h4 mb-4">Cadastro</p>
                    <div class="form-row mb-4">
                    <input type="text" value="<?= set_value('login'); ?>" id="login" class="form-control mb-4" name="login" placeholder="Login">
                    <input type="text" value="<?= set_value('email'); ?>" id="email" class="form-control mb-4" name="email" placeholder="E-mail">
                    <input type="password" value="<?= set_value('senha'); ?>" id="senha" class="form-control mb-4" name="senha" placeholder="Senha">
                    <input type="password" value="<?= set_value('senha2'); ?>" id="senha2" class="form-control mb-4" name="senha2" placeholder="Confirme a sua senha">
                    <button class="btn btn-danger btn-block" type="submit">Cadastrar</button>
                </form>
            </div>
        </div>
    </div>
</div>
